﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;

namespace PokemonBot.Dialog
{
    [LuisModel("83a90618-b819-43ce-afac-8d84356588d8", "3d36e892f5fa4bccadff9c7f23ad5d83")]
    [Serializable]
    public class PokemonDialog : LuisDialog<object>
    {
        [LuisIntent("")]
        public async Task NoIntentFound(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Sorry but I have no idea what you are talking about.");
            context.Wait(MessageReceived);
        }

        [LuisIntent("Greetings")]
        public async Task Greetings(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Well Hello to you too");
            context.Wait(MessageReceived);
        }

        [LuisIntent("Pokemon Info")]
        public async Task PokemonInfo(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("So you want to see a pokemon? Well lets have a little look shall we!");
            context.Wait(MessageReceived);
        }
    }
}